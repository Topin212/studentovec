﻿namespace dialogs.Views
{
    partial class addProfDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SubjectsListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.profNameTextBox = new System.Windows.Forms.TextBox();
            this.profDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.submitButton = new System.Windows.Forms.Button();
            this.profErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.submitButton);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SubjectsListBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.profNameTextBox);
            this.groupBox1.Controls.Add(this.profDateOfBirth);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(340, 235);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add a professor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Subjects";
            // 
            // SubjectsListBox
            // 
            this.SubjectsListBox.FormattingEnabled = true;
            this.SubjectsListBox.Location = new System.Drawing.Point(106, 71);
            this.SubjectsListBox.Name = "SubjectsListBox";
            this.SubjectsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.SubjectsListBox.Size = new System.Drawing.Size(200, 147);
            this.SubjectsListBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date of Birth";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // profNameTextBox
            // 
            this.profNameTextBox.Location = new System.Drawing.Point(106, 19);
            this.profNameTextBox.Name = "profNameTextBox";
            this.profNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.profNameTextBox.TabIndex = 1;
            // 
            // profDateOfBirth
            // 
            this.profDateOfBirth.Location = new System.Drawing.Point(106, 45);
            this.profDateOfBirth.Name = "profDateOfBirth";
            this.profDateOfBirth.Size = new System.Drawing.Size(200, 20);
            this.profDateOfBirth.TabIndex = 0;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(12, 195);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 6;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submit);
            // 
            // profErrorProvider
            // 
            this.profErrorProvider.ContainerControl = this;
            // 
            // addProfDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 254);
            this.Controls.Add(this.groupBox1);
            this.Name = "addProfDialog";
            this.Text = "Add a professor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox profNameTextBox;
        private System.Windows.Forms.DateTimePicker profDateOfBirth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox SubjectsListBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.ErrorProvider profErrorProvider;
    }
}