﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using dialogs.Models;

namespace dialogs.Views
{
    public partial class addProfDialog : Form
    {
        Professor newProf;

        public addProfDialog()
        {
            InitializeComponent();
        }

        private void submit(object sender, EventArgs e)
        {
            string name;
            DateTime dateOfBirth;
            List<string> subjects;

            if((name = profNameTextBox.Text)== "")
                profErrorProvider.SetError(profNameTextBox, "This field cannot be empty.");


            if ((dateOfBirth = profDateOfBirth.Value) == DateTime.Now)
                profErrorProvider.SetError(profDateOfBirth, "This is incorrect.");

            if (SubjectsListBox.SelectedItems.Count == 0 || SubjectsListBox.SelectedItems.Count > 5)
                profErrorProvider.SetError(SubjectsListBox, "This is wrong.");


        }
    }
}
