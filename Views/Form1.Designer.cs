﻿namespace dialogs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.profGroupBox = new System.Windows.Forms.GroupBox();
            this.removeProfessorButton = new System.Windows.Forms.Button();
            this.addProfessorButton = new System.Windows.Forms.Button();
            this.profName = new System.Windows.Forms.Label();
            this.profsComboBox = new System.Windows.Forms.ComboBox();
            this.studentsGroupBox = new System.Windows.Forms.GroupBox();
            this.removeStudentButton = new System.Windows.Forms.Button();
            this.addStudentButton = new System.Windows.Forms.Button();
            this.studentListBox = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imBoredNewFontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDiagramButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.profGroupBox.SuspendLayout();
            this.studentsGroupBox.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // profGroupBox
            // 
            this.profGroupBox.AutoSize = true;
            this.profGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.profGroupBox.Controls.Add(this.removeProfessorButton);
            this.profGroupBox.Controls.Add(this.addProfessorButton);
            this.profGroupBox.Controls.Add(this.profName);
            this.profGroupBox.Controls.Add(this.profsComboBox);
            this.profGroupBox.Location = new System.Drawing.Point(12, 31);
            this.profGroupBox.Name = "profGroupBox";
            this.profGroupBox.Size = new System.Drawing.Size(174, 93);
            this.profGroupBox.TabIndex = 0;
            this.profGroupBox.TabStop = false;
            this.profGroupBox.Text = "Professors";
            // 
            // removeProfessorButton
            // 
            this.removeProfessorButton.Location = new System.Drawing.Point(93, 51);
            this.removeProfessorButton.Name = "removeProfessorButton";
            this.removeProfessorButton.Size = new System.Drawing.Size(75, 23);
            this.removeProfessorButton.TabIndex = 3;
            this.removeProfessorButton.Text = "Remove selected";
            this.removeProfessorButton.UseVisualStyleBackColor = true;
            // 
            // addProfessorButton
            // 
            this.addProfessorButton.Location = new System.Drawing.Point(6, 51);
            this.addProfessorButton.Name = "addProfessorButton";
            this.addProfessorButton.Size = new System.Drawing.Size(81, 23);
            this.addProfessorButton.TabIndex = 2;
            this.addProfessorButton.Text = "Add prof";
            this.addProfessorButton.UseVisualStyleBackColor = true;
            this.addProfessorButton.Click += new System.EventHandler(this.addProffessor);
            // 
            // profName
            // 
            this.profName.AutoSize = true;
            this.profName.Location = new System.Drawing.Point(6, 27);
            this.profName.Name = "profName";
            this.profName.Size = new System.Drawing.Size(35, 13);
            this.profName.TabIndex = 1;
            this.profName.Text = "Name";
            // 
            // profsComboBox
            // 
            this.profsComboBox.FormattingEnabled = true;
            this.profsComboBox.Location = new System.Drawing.Point(47, 24);
            this.profsComboBox.Name = "profsComboBox";
            this.profsComboBox.Size = new System.Drawing.Size(121, 21);
            this.profsComboBox.TabIndex = 0;
            // 
            // studentsGroupBox
            // 
            this.studentsGroupBox.AutoSize = true;
            this.studentsGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.studentsGroupBox.Controls.Add(this.removeStudentButton);
            this.studentsGroupBox.Controls.Add(this.addStudentButton);
            this.studentsGroupBox.Controls.Add(this.studentListBox);
            this.studentsGroupBox.Location = new System.Drawing.Point(198, 31);
            this.studentsGroupBox.Name = "studentsGroupBox";
            this.studentsGroupBox.Size = new System.Drawing.Size(174, 162);
            this.studentsGroupBox.TabIndex = 1;
            this.studentsGroupBox.TabStop = false;
            this.studentsGroupBox.Text = "Students";
            // 
            // removeStudentButton
            // 
            this.removeStudentButton.Location = new System.Drawing.Point(93, 120);
            this.removeStudentButton.Name = "removeStudentButton";
            this.removeStudentButton.Size = new System.Drawing.Size(75, 23);
            this.removeStudentButton.TabIndex = 5;
            this.removeStudentButton.Text = "Remove ";
            this.removeStudentButton.UseVisualStyleBackColor = true;
            // 
            // addStudentButton
            // 
            this.addStudentButton.Location = new System.Drawing.Point(6, 120);
            this.addStudentButton.Name = "addStudentButton";
            this.addStudentButton.Size = new System.Drawing.Size(81, 23);
            this.addStudentButton.TabIndex = 4;
            this.addStudentButton.Text = "Add student";
            this.addStudentButton.UseVisualStyleBackColor = true;
            // 
            // studentListBox
            // 
            this.studentListBox.FormattingEnabled = true;
            this.studentListBox.Location = new System.Drawing.Point(9, 19);
            this.studentListBox.Name = "studentListBox";
            this.studentListBox.Size = new System.Drawing.Size(159, 95);
            this.studentListBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.kekToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(382, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveToToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.fileToolStripMenuItem.ShowShortcutKeys = false;
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.test);
            // 
            // saveToToolStripMenuItem
            // 
            this.saveToToolStripMenuItem.Name = "saveToToolStripMenuItem";
            this.saveToToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveToToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveToToolStripMenuItem.Text = "Save to";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // kekToolStripMenuItem
            // 
            this.kekToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imBoredNewFontToolStripMenuItem});
            this.kekToolStripMenuItem.Name = "kekToolStripMenuItem";
            this.kekToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.kekToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.O)));
            this.kekToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.kekToolStripMenuItem.Text = "Options";
            // 
            // imBoredNewFontToolStripMenuItem
            // 
            this.imBoredNewFontToolStripMenuItem.Name = "imBoredNewFontToolStripMenuItem";
            this.imBoredNewFontToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.imBoredNewFontToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.imBoredNewFontToolStripMenuItem.Text = "I\'m bored, new Font!";
            // 
            // showDiagramButton
            // 
            this.showDiagramButton.Location = new System.Drawing.Point(12, 130);
            this.showDiagramButton.Name = "showDiagramButton";
            this.showDiagramButton.Size = new System.Drawing.Size(174, 56);
            this.showDiagramButton.TabIndex = 3;
            this.showDiagramButton.Text = "ShowDiagram";
            this.showDiagramButton.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 198);
            this.Controls.Add(this.showDiagramButton);
            this.Controls.Add(this.studentsGroupBox);
            this.Controls.Add(this.profGroupBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.profGroupBox.ResumeLayout(false);
            this.profGroupBox.PerformLayout();
            this.studentsGroupBox.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox profGroupBox;
        private System.Windows.Forms.Button addProfessorButton;
        private System.Windows.Forms.Label profName;
        private System.Windows.Forms.ComboBox profsComboBox;
        private System.Windows.Forms.Button removeProfessorButton;
        private System.Windows.Forms.GroupBox studentsGroupBox;
        private System.Windows.Forms.Button removeStudentButton;
        private System.Windows.Forms.Button addStudentButton;
        private System.Windows.Forms.ListBox studentListBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kekToolStripMenuItem;
        private System.Windows.Forms.Button showDiagramButton;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ToolStripMenuItem imBoredNewFontToolStripMenuItem;
    }
}

