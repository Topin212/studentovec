﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dialogs.Models
{
    class Professor
    {
        public string name { get; set; }
        public DateTime dateOfBirth { get; set; }
        public List<string> subjects { get; set; }

        public Professor(string name, DateTime dateOfBirth, List<string> subjects)
        {
            this.name = name;
            this.dateOfBirth = dateOfBirth;
            this.subjects = subjects;
        }
    }
}
