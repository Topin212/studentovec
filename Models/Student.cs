﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dialogs.Models
{
    class Student
    {
        public string name { get; set; }
        public DateTime entranceDate { get; set; }
        public string ProfessorName { get; set; }

        public Student(string name, DateTime entranceDate, string ProfessorName)
        {
            this.name = name;
            this.entranceDate = entranceDate;
            this.ProfessorName = ProfessorName;
        }
    }
}
